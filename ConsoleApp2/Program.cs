﻿using System;
using System.Runtime.InteropServices;
using System.Text;

class Program
{
    [DllImport("kernel32.dll")]
    public static extern uint GetLogicalDrives();

    [DllImport("kernel32.dll")]
    static extern int GetDriveType(string lpRootPathName);

    [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
    static extern bool GetVolumeInformation(
        string rootPathName,
        StringBuilder volumeNameBuffer,
        int volumeNameSize,
        out uint volumeSerialNumber,
        out uint maximumComponentLength,
        out uint fileSystemFlags,
        StringBuilder fileSystemNameBuffer,
        int nFileSystemNameSize);

    [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
    static extern bool GetDiskFreeSpaceEx(
        string lpDirectoryName,
        out ulong lpFreeBytesAvailable,
        out ulong lpTotalNumberOfBytes,
        out ulong lpTotalNumberOfFreeBytes);

    static void Main(string[] args)
    {
        uint drives = GetLogicalDrives();
        Console.WriteLine("Logical drives: {0}", Convert.ToString(drives, 2));

        foreach (char drive in "ABCDEFGHIJKLMNOPQRSTUVWXYZ")
        {
            if ((drives & 1) == 1)
            {
                Console.WriteLine("Drive {0} exists!", drive);

                string rootPathName = String.Format("{0}:\\", drive);
                int driveType = GetDriveType(rootPathName);
                Console.WriteLine("Drive type: {0}", driveType);

                StringBuilder volumeNameBuffer = new StringBuilder(256);
                StringBuilder fileSystemNameBuffer = new StringBuilder(256);
                uint serialNumber, componentLength, flags;

                if (GetVolumeInformation(
                    rootPathName,
                    volumeNameBuffer,
                    volumeNameBuffer.Capacity,
                    out serialNumber,
                    out componentLength,
                    out flags,
                    fileSystemNameBuffer,
                    fileSystemNameBuffer.Capacity))
                {
                    Console.WriteLine("Volume name: {0}", volumeNameBuffer);
                    Console.WriteLine("Serial number: {0:X}", serialNumber);
                    Console.WriteLine("Maximum component length: {0}", componentLength);
                    Console.WriteLine("File system flags: {0:X}", flags);
                    Console.WriteLine("File system name: {0}", fileSystemNameBuffer);
                }
                else
                {
                    Console.WriteLine("GetVolumeInformation error: {0}", Marshal.GetLastWin32Error());
                }

                ulong freeBytesAvailable, totalNumberOfBytes, totalNumberOfFreeBytes;
                if (GetDiskFreeSpaceEx(
                    rootPathName,
                    out freeBytesAvailable,
                    out totalNumberOfBytes,
                    out totalNumberOfFreeBytes))
                {
                    Console.WriteLine("Free bytes available: {0}", freeBytesAvailable);
                    Console.WriteLine("Total number of bytes: {0}", totalNumberOfBytes);
                    Console.WriteLine("Total number of free bytes: {0}", totalNumberOfFreeBytes);
                }
                else
                {
                    Console.WriteLine("GetDiskFreeSpaceEx error: {0}", Marshal.GetLastWin32Error());
                }
            }
            drives >>= 1;
           
        } Console.ReadLine();
    }
}
